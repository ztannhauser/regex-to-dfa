

#include <wchar.h>
#include <stdbool.h>
#include <inttypes.h>


#ifndef _DFA_TUPLE_
#define _DFA_TUPLE_
struct dfa_tuple {
	// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)
	unsigned max_state;    // Q = {0 ... max_state}
	unsigned max_alphabet; // Σ = {0 ... max_alphabet}
	unsigned **table;      // δ = (state, letter) -> (state)
	unsigned start;        // s = starting_state
	bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}
	
	unsigned *values;      // unsigned values[state];
};
#endif // ifndef _DFA_TUPLE_


extern struct dfa_tuple converted_dfa;


struct dfa_tuple converted_dfa = {
	.max_state = 11,
	
	.max_alphabet = 69,
	
	.table = (unsigned**) &(unsigned*[]) {
		[0]     = (unsigned*) &(unsigned[]) {[0 ... 69 + 1] = 0}, // trap state
		[0 + 1] = (unsigned*) &(unsigned[]) {[65] = 1 + 1, },
		[1 + 1] = (unsigned*) &(unsigned[]) {[66] = 2 + 1, [67] = 9 + 1, },
		[2 + 1] = (unsigned*) &(unsigned[]) {[66] = 3 + 1, [67] = 4 + 1, [69] = 5 + 1, },
		[3 + 1] = (unsigned*) &(unsigned[]) {[66] = 3 + 1, [67] = 4 + 1, [69] = 5 + 1, },
		[4 + 1] = (unsigned*) &(unsigned[]) {[67] = 6 + 1, },
		[5 + 1] = (unsigned*) &(unsigned[]) {[67] = 6 + 1, },
		[6 + 1] = (unsigned*) &(unsigned[]) {[67] = 6 + 1, },
		[7 + 1] = (unsigned*) &(unsigned[]) {[66] = 8 + 1, [67] = 7 + 1, },
		[8 + 1] = (unsigned*) &(unsigned[]) {[66] = 10 + 1, [69] = 5 + 1, },
		[9 + 1] = (unsigned*) &(unsigned[]) {[66] = 8 + 1, [67] = 7 + 1, },
		[10 + 1] = (unsigned*) &(unsigned[]) {[66] = 10 + 1, [69] = 5 + 1, },
	},
	
	.start = 0 + 1,
	
	.accepting = (bool*) &(bool[]) {
		[0] = false, // trap state
		[4 + 1] = true,
		[5 + 1] = true,
		[6 + 1] = true,
		[7 + 1] = true,
		[9 + 1] = true,
	},
	
	.values = (unsigned*) &(unsigned[]) {
		[1 ... 11] = 1,
	},
};



