
#include <inttypes.h>

int yychar(int (*read_byte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar);
