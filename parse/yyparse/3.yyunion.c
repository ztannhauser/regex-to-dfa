
#include <stdio.h>

#include <debug.h>

#include <memory/ginc.h>
#include <memory/gdec.h>
#include <memory/gdigraph.h>
#include <memory/delete.h>

#include <state/add_lambda_transition.h>

#include "../yylex/yylex.h"

#include "../token.h"

#include "structs.h"
#include "2.yyjuxtaposition.h"
#include "3.yyunion.h"

int yyunion(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct bundle* outgoing)
{
	int error = 0;
	struct bundle left = {NULL, NULL}, right;
	ENTER;
	
	error = yyjuxtaposition(yybyte, byte, wchar, token, token_data, &left);
	
	while (!error && *token == t_vertical_bar)
	{
		right = (struct bundle) {NULL, NULL};
		
		error = 0
			?: yylex(yybyte, byte, wchar, token, token_data)
			?: yyjuxtaposition(yybyte, byte, wchar, token, token_data, &right)
			?: state_add_lambda_transition(left.start, right.start)
			?: state_add_lambda_transition(right.accept, left.accept);
		
		delete(right.start), delete(right.accept);
		
/*		gdigraph(left.start);*/
/*		CHECK;*/
	}
	
	if (!error)
		*outgoing = (struct bundle) {
			.start = ginc(left.start),
			.accept = ginc(left.accept),
		};
	
	delete(left.start), delete(left.accept);
	
	EXIT;
	return error;
}









