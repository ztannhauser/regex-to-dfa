
#include <stdio.h>
#include <avl.h>
#include <errno.h>

#include <error_codes.h>
#include <debug.h>

#include <memory/ginc.h>
#include <memory/gdigraph.h>
/*#include <memory/gneeds.h>*/
#include <memory/delete.h>

#include <state/struct.h>
#include <state/new.h>
/*#include <state/add_transition.h>*/
#include <state/add_lambda_transition.h>

#include "../token.h"
/*#include "../token_data.h"*/

#include "../yylex/yylex.h"

#include "structs.h"
#include "5.yyrule.h"
#include "yyroot.h"

int yyroot(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct state** retval)
{
	int error = 0;
	ENTER;
	
	if (*token == t_EOF)
	{
		fprintf(stderr, "empty file!\n");
		error = e_syntax_error;
	}
	
	struct state* start = NULL;
	struct state* accept = NULL;
	
	if (!error)
		error = new_state(&start) ?: new_state(&accept);
	
	if (!error)
		accept->is_accepting = true;
	
	struct rule rule;
	while (!error && *token != t_EOF)
	{
		rule = (struct rule) {0, {NULL, NULL}};
		
		error = 0
			?: yyrule(yybyte, byte, wchar, token, token_data, &rule)
			?: state_add_lambda_transition(start, rule.bundle.start)
			?: state_add_lambda_transition(rule.bundle.accept, accept)
			;
		
		if (!error)
			rule.bundle.accept->value = rule.value;
		
		delete(rule.bundle.start), delete(rule.bundle.accept);
	}
	
/*	gdigraph(start);*/
/*	CHECK;*/
	
	if (!error)
		*retval = ginc(start);
	
	delete(start), delete(accept);
	
	EXIT;
	return error;
}


















