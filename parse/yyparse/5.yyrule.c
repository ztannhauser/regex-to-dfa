
#include <stdio.h>

#include <debug.h>
#include <error_codes.h>

#include <memory/ginc.h>
#include <memory/delete.h>

#include "../token.h"
#include "../token_data.h"

#include "../yylex/yylex.h"

#include "structs.h"
#include "4.yyexpression.h"
#include "5.yyrule.h"

int yyrule(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct rule* outgoing)
{
	int error = 0;
	ENTER;
	
	unsigned value = 1;
	if (*token == t_literal)
	{
		value = token_data->literal;
		
		dpv(value);
		
		error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token != t_colon)
			error = e_syntax_error;
		
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
	}
	
	
	struct bundle bundle = {NULL, NULL};
	if (!error)
		error = yyexpression(yybyte, byte, wchar, token, token_data, &bundle);
	
	if (!error && *token == t_semicolon)
		error = yylex(yybyte, byte, wchar, token, token_data);
	
	if (!error)
		*outgoing = (struct rule) {
			.value = value,
			{
				.start = ginc(bundle.start),
				.accept = ginc(bundle.accept)
			}
		};
	
	delete(bundle.start), delete(bundle.accept);
	
	EXIT;
	return error;
}



















