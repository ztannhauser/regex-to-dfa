
#include <stdio.h>
#include <assert.h>
#include <avl.h>

#include <debug.h>
#include <error_codes.h>

#include <macros/cmp.h>

#include <memory/gmalloc.h>
#include <memory/gneeds.h>
#include <memory/gdec.h>
#include <memory/ginc.h>
#include <memory/gdigraph.h>
#include <memory/delete.h>

#include <state/struct.h>
#include <state/new.h>
#include <state/add_transition.h>
#include <state/add_lambda_transition.h>

#include "../yylex/yylex.h"

#include "../token.h"

#include "structs.h"
#include "0.yyhighest.h"
#include "1.yysuffix.h"

static int deep_clone(struct bundle* original, struct bundle* retval)
{
	int error = 0;
	ENTER;
	
	int compare(const void* a, const void* b)
	{
		return cmp(*((struct state**) a), *((struct state**) b));
	}
	
	struct cloning_bundle
	{
		struct state *original; // must be first
		struct state *clone;
	};
	
	struct avl_tree_t* tree = avl_alloc_tree(compare, delete);
	
	dpv(tree);
	
	if (!tree)
		error = e_out_of_memory;
	
	int func(struct state* original, struct state** retval)
	{
		int error = 0;
		size_t i, n;
		struct state* state, *temp;
		struct cloning_bundle* bundle;
		struct transition* transition;
		struct avl_node_t* node = NULL;
		ENTER;
		
		// has this state already been cloned?
		if ((node = avl_search(tree, &original)))
		{
			// return already cloned state
			*retval = ginc(((struct cloning_bundle*) node->item)->clone);
		}
		else
		{
			state = NULL, bundle = NULL;
			
			// create new state, craete new bundle, push to tree
			error = 0
				?: new_state(&state)
				?: gmalloc((void**) &bundle, sizeof(*bundle), NULL)
				?: gneeds(bundle, original)
				?: gneeds(bundle, state)
				;
			
			if (!error)
			{
				bundle->original = original;
				bundle->clone = state;
			}
			
			if (!error)
			{
				if (avl_insert(tree, bundle))
					ginc(bundle);
				else
					error = e_out_of_memory;
			}
			
			// iterate through transitions, cloning destinations
			for (i = 0, n = original->transitions.n; !error && i < n; i++)
			{
				temp = NULL;
				
				transition = original->transitions.data[i];
				
				error = 0
					?: func(transition->to, &temp)
					?: state_add_transition(state, transition->value, temp);
				
				delete(temp);
			}
			
			// iterate through transitions, cloning destinations
			for (i = 0, n = original->lambda_transitions.n; !error && i < n; i++)
			{
				temp = NULL;
				
				error = 0
					?: func(original->lambda_transitions.data[i], &temp)
					?: state_add_lambda_transition(state, temp);
				
				delete(temp);
			}
			
			if (!error)
				*retval = ginc(state);
			
			delete(state), delete(bundle);
		}
		
		EXIT;
		return error;
	}
	
	struct state* start = NULL, *accept = NULL;
	
	if (!error)
		error = func(original->start, &start);
	
/*	struct cloning_bundle* cbundle;*/
/*	struct avl_node_t* node;*/
	
	if (!error)
		accept = ginc(((struct cloning_bundle*) avl_search(tree, &original->accept)->item)->clone);
	
/*	gdigraph2(start, original->start, NULL);*/
/*	CHECK;*/
	
	if (!error)
		*retval = (struct bundle) {
			.start = ginc(start),
			.accept = ginc(accept),
		};
	
	delete(start), delete(accept);
	
	if (tree)
		avl_free_tree(tree);
	
	EXIT;
	return error;
}

int yysuffix(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct bundle* outgoing)
{
	int error = 0;
	struct bundle bundle = {NULL, NULL}, temp;
	ENTER;
	
	error = yyhighest(yybyte, byte, wchar, token, token_data, &bundle);
	
	if (!error)
		switch (*token)
		{
			case t_question:
			{
				error = 0
					?: state_add_lambda_transition(bundle.start, bundle.accept)
					?: yylex(yybyte, byte, wchar, token, token_data)
					;
				
/*				gdigraph(bundle.start);*/
/*				CHECK;*/
				
				break;
			}
			
			case t_plus:
			{
				temp = (struct bundle) {NULL, NULL};
				
				error = deep_clone(&bundle, &temp);
				
				error = 0
					?: state_add_lambda_transition(bundle.accept, temp.start)
					?: state_add_lambda_transition(temp.start, temp.accept)
					?: state_add_lambda_transition(temp.accept, temp.start)
					?: yylex(yybyte, byte, wchar, token, token_data)
					;
				
				if (!error)
				{
					gdec(bundle.accept), bundle.accept = ginc(temp.accept);
				}
				
/*				gdigraph(bundle.start);*/
/*				CHECK;*/
				
				delete(temp.start), delete(temp.accept);
				break;
			}
			
			case t_asterick:
			{
				error = 0
					?: state_add_lambda_transition(bundle.start, bundle.accept)
					?: state_add_lambda_transition(bundle.accept, bundle.start)
					?: yylex(yybyte, byte, wchar, token, token_data)
					;
				
/*				gdigraph(bundle.start);*/
/*				CHECK;*/
				
				break;
			}
			
			default:
				break;
		}
	
	if (!error)
		*outgoing = (struct bundle) {
			.start = ginc(bundle.start),
			.accept = ginc(bundle.accept),
		};
	
	delete(bundle.start), delete(bundle.accept);
	
	EXIT;
	return error;
}


















