
#include <stdio.h>

#include <debug.h>

/*#include "../token.h"*/

/*#include "structs.h"*/
#include "3.yyunion.h"
#include "4.yyexpression.h"

int yyexpression(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct bundle* outgoing)
{
	int error = 0;
	ENTER;
	
	error = yyunion(yybyte, byte, wchar, token, token_data, outgoing);
	
	EXIT;
	return error;
}
