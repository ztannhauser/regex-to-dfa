
#include <stdio.h>
#include <stdbool.h>

#include <debug.h>

#include <memory/ginc.h>
#include <memory/gdec.h>
#include <memory/gdigraph.h>
#include <memory/delete.h>

#include <state/add_lambda_transition.h>

#include "../token.h"

#include "structs.h"
#include "1.yysuffix.h"
#include "2.yyjuxtaposition.h"

int yyjuxtaposition(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct bundle* outgoing)
{
	int error = 0;
	ENTER;
	
	struct bundle left = {NULL, NULL};
	
	error = yysuffix(yybyte, byte, wchar, token, token_data, &left);
	
	struct bundle right;
	
	while (!error && (false
		|| *token == t_string
		|| *token == t_open_square
		|| *token == t_open_paren
		|| *token == t_period))
	{
		right = (struct bundle) {NULL, NULL};
		
		error = 0
			?: yysuffix(yybyte, byte, wchar, token, token_data, &right)
			?: state_add_lambda_transition(left.accept, right.start);
		
		if (!error)
			gdec(left.accept), left.accept = ginc(right.accept);
		
		delete(right.start), delete(right.accept);
		
	}
	
/*	gdigraph(left.start);*/
/*	CHECK;*/
	
	if (!error)
		*outgoing = (struct bundle) {
			.start = ginc(left.start),
			.accept = ginc(left.accept),
		};
	
	delete(left.start),  delete(left.accept);
	
	EXIT;
	return error;
}





















