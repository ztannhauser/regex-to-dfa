
struct rule
{
	unsigned value;
	
	struct bundle
	{
		struct state *start;
		struct state *accept;
	} bundle;
};
