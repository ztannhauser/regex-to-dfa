
#include <stdbool.h>
#include <stdio.h>

#include <error_codes.h>
#include <debug.h>

#include <memory/ginc.h>
#include <memory/gdec.h>
#include <memory/gdigraph.h>
#include <memory/delete.h>

#include <state/new.h>
#include <state/add_transition.h>
#include <state/set_default_transition.h>

#include "../yylex/yylex.h"

#include "../token.h"
#include "../token_data.h"

#include "structs.h"
#include "4.yyexpression.h"
#include "0.yyhighest.h"

static int yystring(wchar_t* string, struct bundle* outgoing)
{
	int error = 0;
	struct state* start = NULL;
	struct state* accept = NULL;
	struct state* temp;
	ENTER;
	
	error = new_state(&start), accept = ginc(start);
	
	while (!error && *string)
	{
		temp = NULL;
		
		error = 0
			?: new_state(&temp)
			?: state_add_transition(accept, *string++, temp)
			;
		
		if (!error)
			gfree(accept), accept = ginc(temp);
		
		delete(temp);
	}
	
/*	gdigraph(start);*/
/*	CHECK;*/
	
	if (!error)
		*outgoing = (struct bundle) {
			.start = ginc(start),
			.accept = ginc(accept),
		};
	
	delete(start), delete(accept);
	
	EXIT;
	return error;
}

static int yyperiod(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct bundle* outgoing)
{
	int error = 0;
	ENTER;
	
	struct state* start = NULL;
	struct state* accept = NULL;
	
	error = 0
		?: new_state(&start) ?: new_state(&accept)
		?: state_set_default_transition(start, accept)
		?: yylex(yybyte, byte, wchar, token, token_data)
		;
	
/*	gdigraph(start);*/
/*	CHECK;*/
	
	if (!error)
		*outgoing = (struct bundle) {
			.start = ginc(start),
			.accept = ginc(accept),
		};
	
	delete(start), delete(accept);
	
	EXIT;
	return error;
}

static int yyrange(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct bundle* outgoing)
{
	int error = 0;
	ENTER;
	
	assert(*token == t_open_square);
	error = yylex(yybyte, byte, wchar, token, token_data);
	
	bool negate = false;
	if (!error && *token == t_carrot)
	{
		negate = false;
		error = yylex(yybyte, byte, wchar, token, token_data);
	}
	
	if (!error && *token == t_close_square)
		error = e_syntax_error;
	
	struct state* start = NULL;
	struct state* accept = NULL;
	struct state* reject = NULL;
	
	error = new_state(&start) ?: new_state(&accept) ?: new_state(&reject);
	
	if (!error)
		error = state_set_default_transition(start, reject);
	
	unsigned low, high;
	while (!error && *token != t_close_square)
	{
		if (*token != t_literal)
			error = e_syntax_error;
		
		if (!error)
		{
			low = token_data->literal;
			error = yylex(yybyte, byte, wchar, token, token_data);
		}
		
		dpvc(low);
		
		if (!error)
		{
			if (*token == t_minus)
			{
				error = yylex(yybyte, byte, wchar, token, token_data);
				
				if (*token != t_literal)
					error = e_syntax_error;
				
				if (!error)
					high = token_data->literal;
				
				dpvc(high);
				
				while (!error && low + 1 <= high + 1)
					error = state_add_transition(start, low++, accept);
				
				if (!error)
					error = yylex(yybyte, byte, wchar, token, token_data);
				
			}
			else
			{
				error = state_add_transition(start, low, accept);
			}
		}
		
		if (!error && *token == t_comma)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
	}
	
	if (!error)
		error = yylex(yybyte, byte, wchar, token, token_data);
	
/*	gdigraph(start);*/
/*	CHECK;*/
	
	if (!error)
		*outgoing = (struct bundle) {
			.start = ginc(start),
			.accept = negate ? ginc(reject) : ginc(accept),
		};
	
	delete(start), delete(accept), delete(reject);
	
	EXIT;
	return error;
}

int yyhighest(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct bundle* outgoing)
{
	int error = 0;
	ENTER;
	
	switch (*token)
	{
		case t_open_paren:
		{
			error = 0
				?: yylex(yybyte, byte, wchar, token, token_data)
				?: yyexpression(yybyte, byte, wchar, token, token_data, outgoing);
			
			if (!error && *token != t_close_paren)
				error = e_syntax_error;
			
			if (!error)
				error = yylex(yybyte, byte, wchar, token, token_data);
			
			break;
		}
		
		case t_string:
		{
			wchar_t* string = token_data->string;
			
			error = 0
				?: yystring(token_data->string, outgoing)
				?: yylex(yybyte, byte, wchar, token, token_data);
			
			delete(string);
			break;
		}
		
		case t_period:
			error = yyperiod(yybyte, byte, wchar, token, token_data, outgoing);
			break;
		
		case t_open_square:
			error = yyrange(yybyte, byte, wchar, token, token_data, outgoing);
			break;
		
		default:
			dpv(*token);
			CHECK;
			error = e_syntax_error;
			break;
	}
	
	EXIT;
	return error;
}










