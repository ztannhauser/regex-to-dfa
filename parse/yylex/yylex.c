
#include <stdbool.h>
#include <wchar.h>
#include <errno.h>
#include <limits.h>

#include <error_codes.h>
#include <debug.h>

#include <macros/min.h>

#include <memory/grealloc.h>
#include <memory/ginc.h>
#include <memory/delete.h>

#include "../yychar/yychar.h"

#include "../token.h"
#include "../token_data.h"

#include "yylex.h"

// 128 represents unicode
#define ALL 0 ... 128

static const enum state {
	s_error = 0,
	
	s_first_token = 1,
	
	s_last_token = number_of_tokens - 1,
	
	s_its_an_identifier,
	s_its_a_string_literal,
	s_its_a_character_literal,
	s_its_a_integer_literal,
	
	s_start,
	
	s_after_open_square,
	s_after_close_square,
	s_after_open_paren,
	s_after_close_paren,
	
	s_after_plus,
	s_after_colon,
	s_after_minus,	
	s_after_comma,
	s_after_period,
	s_after_carrot,
	s_after_question,
	s_after_asterick,
	s_after_semicolon,
	s_after_vertical_bar,
	
	s_after_slash,
	s_ignore_until_newline,
	s_ignore_until_close_slash,
	s_ignore_until_close_slash2,
	
	number_of_states,
} table[number_of_states][127 + 1 + 1] = {
	
	// EOF:
	[s_start]['\0'] = t_EOF,
	
	// whitespace:
	[s_start][' ']  = s_start,
	[s_start]['\r'] = s_start,
	[s_start]['\n'] = s_start,
	[s_start]['\t'] = s_start,
	
	// comments:
	[s_start]['/'] = s_after_slash,
		[s_after_slash]['*'] = s_ignore_until_close_slash,
			[s_ignore_until_close_slash][ALL] = s_ignore_until_close_slash,
			[s_ignore_until_close_slash]['*'] = s_ignore_until_close_slash2,
			[s_ignore_until_close_slash2][ALL] = s_ignore_until_close_slash,
			[s_ignore_until_close_slash2]['*'] = s_ignore_until_close_slash2,
			[s_ignore_until_close_slash2]['/'] = s_start,
		[s_after_slash]['/'] = s_ignore_until_newline,
			[s_ignore_until_newline][ALL] = s_ignore_until_newline,
			[s_ignore_until_newline]['\n'] = s_start,
	
	// literal:
	[s_start]['\''] = s_its_a_character_literal,
	[s_start]['0' ... '9'] = s_its_a_integer_literal,
	
	// string:
	[s_start]['\"'] = s_its_a_string_literal,
	
	// identifier:
	[s_start]['_'] = s_its_an_identifier,
	[s_start][128] = s_its_an_identifier,
	[s_start]['a' ... 'z'] = s_its_an_identifier,
	[s_start]['A' ... 'Z'] = s_its_an_identifier,
	
	// brackets:
	[s_start]['['] = s_after_open_square,  [s_after_open_square][ALL]  = t_open_square,
	[s_start][']'] = s_after_close_square, [s_after_close_square][ALL] = t_close_square,
	[s_start]['('] = s_after_open_paren,   [s_after_open_paren][ALL]   = t_open_paren,
	[s_start][')'] = s_after_close_paren,  [s_after_close_paren][ALL]  = t_close_paren,
	
	// single-letters:
	[s_start]['+'] = s_after_plus,         [s_after_plus][ALL]         = t_plus,
	[s_start][':'] = s_after_colon,        [s_after_colon][ALL]        = t_colon,
	[s_start][','] = s_after_comma,        [s_after_comma][ALL]        = t_comma,
	[s_start]['-'] = s_after_minus,        [s_after_minus][ALL]        = t_minus,
	[s_start]['.'] = s_after_period,       [s_after_period][ALL]       = t_period,
	[s_start]['^'] = s_after_carrot,       [s_after_carrot][ALL]       = t_carrot,
	[s_start]['?'] = s_after_question,     [s_after_question][ALL]     = t_question,
	[s_start]['*'] = s_after_asterick,     [s_after_asterick][ALL]     = t_asterick,
	[s_start][';'] = s_after_semicolon,    [s_after_semicolon][ALL]    = t_semicolon,
	[s_start]['|'] = s_after_vertical_bar, [s_after_vertical_bar][ALL] = t_vertical_bar,
};

static int yylex_character_literal(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar, enum token* token, union token_data* token_data)
{
	int error = 0;
	ENTER;
	
	assert(*wchar == L'\'');
	
	error = yychar(yybyte, byte, wchar);
	
	dpvc(*wchar);
	
	if (*wchar == L'\\')
	{
		error = yychar(yybyte, byte, wchar);
		
		if (!error)
			switch (*wchar)
			{
				case L'0': *wchar = L'\0'; break;
				case L'r': *wchar = L'\r'; break;
				case L'n': *wchar = L'\n'; break;
				case L't': *wchar = L'\t'; break;
				
				default:
					error = e_syntax_error;
					break;
			}
	}
	
	if (!error)
	{
		*token = t_literal;
		token_data->literal = *wchar;
		error = yychar(yybyte, byte, wchar);
	}
	
	dpv(token_data->literal);
	
	if (*wchar != L'\'')
		error = e_syntax_error;
	
	if (!error)
		error = yychar(yybyte, byte, wchar);
	
	EXIT;
	return error;
}

static int yylex_integer_literal(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar, enum token* token, union token_data* token_data)
{
	int error = 0;
	char* buffer = NULL, *m;
	size_t n = 0, cap = 0;
	ENTER;
	
	int push(char wc)
	{
		int error = 0;
		
		if (n + 1 > cap)
			error = grealloc((void**) &buffer, (cap = cap * 2 ?: 1));
		
		buffer[n++] = wc;
		
		return error;
	}
	
	dpv(*wchar);
	
	while (!error && '0' <= *wchar && *wchar <= '9')
	{
		error = 0
			?: push(*wchar)
			?: yychar(yybyte, byte, wchar);
	}
	
	if (!error)
		error = push('\0');
	
	unsigned long int value;
	
	if (!error)
	{
		value = strtoul(buffer, &m, 0);
		
		if (*m || errno || value > UINT_MAX)
			error = e_syntax_error;
	}
	
	dpv(value);
	
	if (!error)
	{
		*token = t_literal;
		token_data->literal = value;
	}
	
	delete(buffer);
	
	EXIT;
	return error;
}

static int yylex_string(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar, enum token* token, union token_data* token_data)
{
	int error = 0;
	wchar_t* buffer = NULL;
	size_t n = 0, cap = 0;
	ENTER;
	
	assert(*wchar == L'\"');
	
	int push(wchar_t wc)
	{
		int error = 0;
		
		if (n + 1 > cap)
			error = grealloc((void**) &buffer, sizeof(wchar_t) * (cap = cap * 2 ?: 1));
		
		buffer[n++] = wc;
		
		return error;
	}
	
	dpv(*wchar);
	
	error = yychar(yybyte, byte, wchar);
	
	while (!error && *wchar != L'\"')
	{
		error = 0
			?: push(*wchar)
			?: yychar(yybyte, byte, wchar);
	}
	
	if (!error)
		error =  yychar(yybyte, byte, wchar);
	
	if (!error)
		error = push(L'\0');
	
	if (!error)
	{
		*token = t_string;
		token_data->string = ginc(buffer);
	}
	
	delete(buffer);
	
	EXIT;
	return error;
}

static int yylex_identifier(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar, enum token* token, union token_data* token_data)
{
	int error = 0;
	wchar_t* buffer = NULL;
	size_t n = 0, cap = 0;
	ENTER;
	
	int push(wchar_t wc)
	{
		int error = 0;
		
		if (n + 1 > cap)
			error = grealloc((void**) &buffer, sizeof(wchar_t) * (cap = cap * 2 ?: 1));
		
		buffer[n++] = wc;
		
		return error;
	}
	
	dpv(*wchar);
	
	while (!error && (false
		|| *wchar > 127
		|| wcschr(L"_"
			L"abcdefghijklmnnopqrstuvwxyz"
			L"ABCDEFGHIJKLMNNOPQRSTUVWXYZ", *wchar)))
	{
		error = 0
			?: push(*wchar)
			?: yychar(yybyte, byte, wchar);
	}
	
	if (!error)
		error = push(L'\0');
	
	dpv(error);
	
	if (!error)
	{
		*token = t_identifier;
		token_data->identifier = ginc(buffer);
	}
	
	delete(buffer);
	
	EXIT;
	return error;
}

int yylex(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar, enum token* token, union token_data* token_data)
{
	int error = 0;
	ENTER;
	
	dpv(*wchar);
	
	enum state state = s_start;
	
	while (!error && (state = table[state][min(*wchar, 128)]) >= s_start)
		error = yychar(yybyte, byte, wchar);
	
	if (!error)
		switch (state)
		{
			case s_error:
				TODO;
				break;
			
			case s_its_an_identifier:
				error = yylex_identifier(yybyte, byte, wchar, token, token_data);
				break;
			
			case s_its_a_string_literal:
				error = yylex_string(yybyte, byte, wchar, token, token_data);
				break;
			
			case s_its_a_character_literal:
				error = yylex_character_literal(yybyte, byte, wchar, token, token_data);
				break;
			
			case s_its_a_integer_literal:
				error = yylex_integer_literal(yybyte, byte, wchar, token, token_data);
				break;
			
			case s_first_token ... s_last_token:
				*token = (enum token) state;
				break;
			
			default:
				TODO;
				break;
		}
	
	EXIT;
	return error;
}


























