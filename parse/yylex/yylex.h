
#include <wchar.h>
#include <inttypes.h>

enum token;
union token_data;

int yylex(int (*read_byte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar, enum token* token, union token_data* token_data);
