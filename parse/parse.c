
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <error_codes.h>
#include <debug.h>

#include "token.h"
#include "token_data.h"

#include "yychar/yychar.h"
#include "yylex/yylex.h"
#include "yyparse/yyroot.h"

#include "parse.h"

int parse(struct state** retval, const char* path)
{
	int error = 0;
	int fd = -1;
	ENTER;
	
	if (!path)
		error = e_bad_command_line_args;
	
	if (!error && (fd = open(path, O_RDONLY)) < 0)
		perror("open"),
		error = e_syscall_failed;
	
	dpv(fd);
	
	uint8_t buffer[4096];
	off_t index = 0, size = 0;
	off_t offset = 0;
	
	int yybyte(uint8_t* byte)
	{
		int error = 0;
		ENTER;
		
		if (index < size)
			*byte = buffer[index++];
		else if ((size = pread(fd, buffer, sizeof(buffer), offset)) < 0)
			perror("pread"),
			error = e_syscall_failed;
		else if (!size)
			*byte = '\0';
		else
			index = 0, offset += size,
			*byte = buffer[index++];
		
		dpv(index);
		dpv(*byte);
		dpv(size);
		
		EXIT;
		return error;
	}
	
	uint8_t byte;
	wchar_t wchar;
	enum token token;
	union token_data token_data;
	
	if (!error)
		error = 0
			?: yybyte(&byte)
			?: yychar(yybyte, &byte, &wchar)
			?: yylex(yybyte, &byte, &wchar, &token, &token_data)
			?: yyroot(yybyte, &byte, &wchar, &token, &token_data, retval);
	
	if (fd > 0)
		close(fd);
	
	EXIT;
	return error;
}
















