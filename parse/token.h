
enum token
{
	t_error = 0,
	t_EOF,
	
	t_period,
	
	t_plus,
	t_asterick,
	
	t_colon,
	t_comma,
	t_question,
	
	t_carrot,
	t_semicolon,
	t_vertical_bar,
	
	t_open_paren,
	t_close_paren,
	t_open_square,
	t_close_square,
	
	t_minus,
	
	t_literal,
	t_string,
	t_identifier,
	
	
	number_of_tokens,
};

