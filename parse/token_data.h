
#include <wchar.h>

union token_data
{
	wchar_t* identifier;
	wchar_t* string;
	unsigned literal;
};

