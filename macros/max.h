#define max(a, b) ({ typeof(a) _a = (a), _b = (b); _a > _b ? _a : _b; })
