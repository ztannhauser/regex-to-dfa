
#include <error_codes.h>
#include <debug.h>

#include "how_many.h"

int utf8_how_many(int* retval, uint8_t header)
{
	int error = 0;
	ENTER;
	
	dpv(header);
	
	switch (header)
	{
		case 0b00000000 ... 0b01111111: *retval = 1; break;
		case 0b11000000 ... 0b11011111: *retval = 2; break;
		case 0b11100000 ... 0b11101111: *retval = 3; break;
		case 0b11110000 ... 0b11110111: *retval = 4; break;
		case 0b11111000 ... 0b11111011: *retval = 5; break;
		case 0b11111100 ... 0b11111101: *retval = 6; break;
		default: error = e_malformed_utf8;
	}
	
	dpv(*retval);
	
	EXIT;
	return error;
}

