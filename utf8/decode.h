
#include <wchar.h>
#include <inttypes.h>

int utf8_decode(wchar_t* retval, uint8_t buffer[6]);
