
#include <wchar.h>
#include <inttypes.h>

int utf8_encode(uint8_t* utf8_out, wchar_t unicode);
