

#include <wchar.h>
#include <stdbool.h>
#include <inttypes.h>


#ifndef _DFA_TUPLE_
#define _DFA_TUPLE_
struct dfa_tuple {
	// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)
	unsigned max_state;    // Q = {0 ... max_state}
	unsigned max_alphabet; // Σ = {0 ... max_alphabet}
	unsigned **table;      // δ = (state, letter) -> (state)
	unsigned start;        // s = starting_state
	bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}
	
	unsigned *values;      // unsigned values[state];
};
#endif // ifndef _DFA_TUPLE_


extern struct dfa_tuple converted_dfa;


struct dfa_tuple converted_dfa = {
	.max_state = 16,
	
	.max_alphabet = 98,
	
	.table = (unsigned**) &(unsigned*[]) {
		[0]     = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 0}, // trap state
		[0 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 15 + 1, [97] = 1 + 1, [98] = 14 + 1, },
		[1 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, [97] = 2 + 1, [98] = 4 + 1, },
		[2 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 11 + 1, [97] = 3 + 1, [98] = 13 + 1, },
		[3 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, [97] = 2 + 1, [98] = 4 + 1, },
		[4 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 11 + 1, [97] = 5 + 1, [98] = 7 + 1, },
		[5 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, [97] = 6 + 1, [98] = 12 + 1, },
		[6 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 11 + 1, [97] = 5 + 1, [98] = 7 + 1, },
		[7 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, [97] = 8 + 1, },
		[8 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 11 + 1, [97] = 9 + 1, },
		[9 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, [97] = 8 + 1, },
		[10 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 11 + 1, },
		[11 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, },
		[12 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 11 + 1, [97] = 9 + 1, },
		[13 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, [97] = 6 + 1, [98] = 12 + 1, },
		[14 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, [97] = 6 + 1, [98] = 12 + 1, },
		[15 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 10 + 1, },
	},
	
	.start = 0 + 1,
	
	.accepting = (bool*) &(bool[]) {
		[0] = false, // trap state
		[1 + 1] = true,
		[3 + 1] = true,
		[5 + 1] = true,
		[7 + 1] = true,
		[8 + 1] = true,
		[9 + 1] = true,
		[11 + 1] = true,
		[12 + 1] = true,
		[13 + 1] = true,
		[14 + 1] = true,
		[15 + 1] = true,
	},
	
	.values = (unsigned*) &(unsigned[]) {
		[1 ... 16] = 1,
		[1 + 1] = 3,
		[3 + 1] = 3,
		[5 + 1] = 3,
		[7 + 1] = 6,
		[8 + 1] = 2,
		[9 + 1] = 6,
		[11 + 1] = 3,
		[12 + 1] = 2,
		[13 + 1] = 3,
		[14 + 1] = 3,
		[15 + 1] = 3,
	},
};



