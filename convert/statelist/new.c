
#include <debug.h>

#include <memory/gmalloc.h>
#include <memory/ginc.h>
#include <memory/delete.h>

#include "struct.h"
#include "new.h"

void delete_statelist(void* ptr)
{
	struct statelist* this = ptr;
	gfree(this->states);
}

int new_statelist(struct statelist** new)
{
	int error = 0;
	ENTER;
	
	struct statelist* this = NULL;
	
	error = gmalloc((void**) &this, sizeof(*this), delete_statelist);
	
	if (!error)
	{
		this->states = NULL;
		this->n = 0;
		this->cap = 0;
	}
	
	if (!error)
		*new = ginc(this);
	
	delete(this);
	
	EXIT;
	return error;
}


