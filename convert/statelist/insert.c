
#include <debug.h>

#include <memory/grealloc.h>
#include <memory/gneeds.h>

#include "compare.h"
#include "struct.h"
#include "contains.h"
#include "insert.h"

int statelist_insert(struct statelist* this, struct state* state)
{
	int error = 0;
	size_t i;
	ENTER;
	
	dpv(this);
	dpv(state);
	
	if (!statelist_contains(this, state))
	{
		if (this->n + 1 >= this->cap)
		{
			error = grealloc((void**) &this->states, sizeof(*this->states) * (this->cap = this->cap * 2 ?: 1));
		}
		
		if (!error)
			error = gneeds(this, state);
		
		if (!error)
		{
			for (i = this->n - 1; i + 1 >= 0 + 1 && compare_states(&this->states[i], &state) > 0; i--)
				this->states[i + 1] = this->states[i];
			
			dpv(i + 1);
			
			this->states[i + 1] = state, this->n++;
		}
	}
	
	#if 0
	{
		size_t i, n;
		for (i = 0, n = this->n; i < n; i++)
		{
			dprintf("states[%lu] == %p\n", i, this->states[i]);
		}
		
		for (i = 1, n = this->n; i < n; i++)
		{
			if (!(this->states[i - 1] < this->states[i]))
			{
				CHECK;
			}
		}
	}
	
	usleep(100 * 1000);
	#endif
	
	EXIT;
	return error;
}














