
#include <debug.h>

#include "struct.h"
#include "compare.h"
#include "contains.h"

bool statelist_contains(struct statelist* this, struct state* state)
{
	bool retval;
	ENTER;
	
	dpv(this);
	dpv(state);
	
	retval = !!bsearch(&state, this->states, this->n, sizeof(*this->states), compare_states);
	
	dpvb(retval);
	
	EXIT;
	return retval;
}

