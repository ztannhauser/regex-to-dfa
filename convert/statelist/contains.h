
#include <stdbool.h>

struct statelist;
struct state;

bool statelist_contains(struct statelist* this, struct state* state);
