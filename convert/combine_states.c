
#include <avl.h>

#include <debug.h>
#include <error_codes.h>

#include <state/struct.h>
#include <state/new.h>
#include <state/add_transition.h>

#include <memory/gmalloc.h>
#include <memory/grealloc.h>
#include <memory/gneeds.h>
#include <memory/ginc.h>
#include <memory/delete.h>

#include "statelist/struct.h"
#include "statelist/new.h"
#include "statelist/insert.h"

#include "bundle.h"
#include "push_lambda_transitions.h"
#include "combine_states.h"

int combine_states(struct avl_tree_t* tree, struct statelist* statelist, struct state** retval)
{
	int error = 0;
	avl_node_t* node;
	ENTER;
	
	dpv(statelist->n);
	
	// have we done this list before?
	if ((node = avl_search(tree, &statelist)))
	{
		struct bundle* bundle;
		// return reference to already done state
		
		dprintf("already done!\n");
		
		bundle = node->item;
		
		*retval = ginc(bundle->combined);
	}
	else
	{
		size_t i, n;
/*		struct state** moving;*/
		struct state* combined_state = NULL;
		
		// create new state, craete new bundle, push to tree
		{
			wchar_t* name = NULL;
			struct bundle* bundle = NULL;
			
			error = 0
				?: new_state(&combined_state)
				?: gmalloc((void**) &bundle, sizeof(*bundle), NULL)
				?: gneeds(bundle, statelist)
				?: gneeds(bundle, combined_state);
			
			if (!error)
			{
				bundle->statelist = statelist;
				bundle->combined = combined_state;
				
				node = avl_insert(tree, bundle);
				
				if (!node)
					error = e_out_of_memory;
			}
			
			if (error)
				delete(bundle);
			
			delete(name);
		}
		
		// set this as accepting if any states in list are accepting
		if (!error)
		{
			for (i = 0, n = statelist->n; !combined_state->is_accepting && i < n; i++)
				if (statelist->states[i]->is_accepting)
					combined_state->is_accepting = true;
			
			dpvb(combined_state->is_accepting);
		}
		
		// set this state's value to the product of dfa states:
		if (!error)
		{
			for (i = 0, n = statelist->n; i < n; i++)
				combined_state->value *= statelist->states[i]->value;
			
			dpv(combined_state->value);
		}
		
		struct state* state;
		struct transition* transition;
		
		// create transition iterators for each:
		struct iterator {
			struct transition ** moving, **end;
		} (*iterators)[statelist->n] = NULL, *iterator;
		
		size_t iterators_n = 0;
		
		if (!error)
		{
			error = gmalloc((void**) &iterators, sizeof(*iterators), NULL);
			
			if (!error)
				for (i = 0, n = statelist->n; i < n; i++)
				{
					state = statelist->states[i];
					
					if (state->transitions.n)
						iterators_n++;
					
					(*iterators)[i] = (struct iterator) {
						.moving = state->transitions.data,
						.end = state->transitions.data + state->transitions.n
					};
				}
		}
		
/*		size_t N;*/
/*		size_t j, m;*/
		unsigned lowest;
		struct state* transition_combined_state;
		struct statelist* transition_statelist;
		// until there are no more transitions:
		for (size_t N = iterators_n; !error && N > 0; )
		{
			HERE;
			dpv(N);
			
			transition_statelist = NULL;
			
			dpv(statelist->n);
			HERE;
			
			// what's the next lowest transition value in all states in list?
			for (lowest = -1, i = 0, n = statelist->n; i < n; i++)
			{
				iterator = &((*iterators)[i]);
				
				dpv(iterator->moving);
				dpv(iterator->end);
				
				if (iterator->moving < iterator->end)
				{
					transition = *(*iterators)[i].moving;
					
					dpv(transition->value);
					
					if (transition->value < lowest)
						lowest = transition->value;
				}
			}
			
			dpv(lowest);
			
/*			CHECK;*/
			
			error = new_statelist(&transition_statelist);
			
			// make a list of all the new places we would be,
			for (i = 0, n = statelist->n; !error && i < n; i++)
			{
				iterator = &((*iterators)[i]);
				
				if (iterator->moving < iterator->end && iterator->moving[0]->value == lowest)
				{
					error = statelist_insert(transition_statelist, iterator->moving[0]->to);
					
					if (++iterator->moving == iterator->end)
						N--;
				}
				else if ((state = statelist->states[i]->default_transition_to))
				{
					error = statelist_insert(transition_statelist, state);
				}
			}
			
			dpv(transition_statelist->n);
			
/*			CHECK;*/
			
			// if nonempty list:
			if (!error && transition_statelist->n > 0)
			{
				transition_combined_state = NULL;
				
				dpv(transition_statelist->n);
				
				// add lambda transitions to elements in list
				error = push_lambda_transitions(transition_statelist);
				
				dpv(transition_statelist->n);
				
				// call convert(on that new list)
				if (!error)
					error = combine_states(tree, transition_statelist, &transition_combined_state);
				
				// add new transition to new state to go to returned state
				if (!error)
					error = state_add_transition(combined_state, lowest, transition_combined_state);
				
				delete(transition_combined_state);
			}
			
/*			CHECK;*/
			
			delete(transition_statelist);
		}
		
		// call convert on the list of all default-transitions, set result to
		// new state's default transition
		if (!error)
		{
			transition_statelist = NULL;
			
			error = new_statelist(&transition_statelist);
			
			for (i = 0, n = statelist->n; !error && i < n; i++)
				if ((state = statelist->states[i]->default_transition_to))
					error = statelist_insert(transition_statelist, state);
			
/*			CHECK;*/
			
			// if nonempty list:
			if (!error && transition_statelist->n > 0)
			{
				transition_combined_state = NULL;
				
				dpv(transition_statelist->n);
				
				// add lambda transitions to elements in list
				error = push_lambda_transitions(transition_statelist);
				
				dpv(transition_statelist->n);
				
				// call convert(on that new list)
				if (!error)
					error = combine_states(tree, transition_statelist, &transition_combined_state);
				
				// add new transition to new state to go to returned state
				if (!error)
				{
					combined_state->default_transition_to = transition_combined_state;
					error = gneeds(combined_state, transition_combined_state);
				}
				
				delete(transition_combined_state);
			}
			
			delete(transition_statelist);
		}
		
		if (!error)
			*retval = ginc(combined_state);
		
		delete(iterators);
		delete(combined_state);
	}
	
	EXIT;
	return error;
}























