
#include <avl.h>

#include <debug.h>
#include <error_codes.h>

#include <memory/gmalloc.h>
#include <memory/delete.h>

#include "statelist/new.h"
#include "statelist/insert.h"
#include "push_lambda_transitions.h"

#include "compare_bundles.h"
#include "combine_states.h"
#include "convert.h"

int convert(struct state* start_state, struct state** retval)
{
	int error = 0;
	avl_tree_t* tree = NULL;
	struct statelist* starting_statelist = NULL;
	ENTER;
	
	// tree maps list of dfa states to nfa states
	if (!(tree = avl_alloc_tree(compare_bundles, delete)))
		error = e_out_of_memory;
	
	// setup initial state-list:
	if (!error)
		error = 0
			?: new_statelist(&starting_statelist)
			?: statelist_insert(starting_statelist, start_state)
			?: push_lambda_transitions(starting_statelist);
	
	// start combining states:
	if (!error)
		error = combine_states(tree, starting_statelist, retval);
	
	delete(starting_statelist);
	
	if (tree)
		avl_free_tree(tree);
	
	EXIT;
	return error;
}

















