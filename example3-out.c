

#include <wchar.h>
#include <stdbool.h>
#include <inttypes.h>


#ifndef _DFA_TUPLE_
#define _DFA_TUPLE_
struct dfa_tuple {
	// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)
	unsigned max_state;    // Q = {0 ... max_state}
	unsigned max_alphabet; // Σ = {0 ... max_alphabet}
	unsigned **table;      // δ = (state, letter) -> (state)
	unsigned start;        // s = starting_state
	bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}
	
	unsigned *values;      // unsigned values[state];
};
#endif // ifndef _DFA_TUPLE_


extern struct dfa_tuple converted_dfa;


struct dfa_tuple converted_dfa = {
	.max_state = 17,
	
	.max_alphabet = 99,
	
	.table = (unsigned**) &(unsigned*[]) {
		[0]     = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 0}, // trap state
		[0 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 15 + 1, [65] = 1 + 1, [66] = 3 + 1, [67] = 11 + 1, },
		[1 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 15 + 1, [65] = 1 + 1, [66] = 3 + 1, [67] = 11 + 1, [97] = 16 + 1, },
		[2 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, [98] = 7 + 1, },
		[3 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 8 + 1, [66] = 4 + 1, [97] = 5 + 1, [98] = 2 + 1, },
		[4 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 8 + 1, [66] = 4 + 1, [97] = 5 + 1, [98] = 2 + 1, },
		[5 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, },
		[6 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, },
		[7 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, [98] = 7 + 1, },
		[8 + 1] = (unsigned*) &(unsigned[]) {},
		[9 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, [99] = 9 + 1, },
		[10 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, [99] = 9 + 1, },
		[11 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 14 + 1, [67] = 12 + 1, [97] = 13 + 1, [99] = 10 + 1, },
		[12 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 14 + 1, [67] = 12 + 1, [97] = 13 + 1, [99] = 10 + 1, },
		[13 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, },
		[14 + 1] = (unsigned*) &(unsigned[]) {},
		[15 + 1] = (unsigned*) &(unsigned[]) {},
		[16 + 1] = (unsigned*) &(unsigned[]) {[97] = 6 + 1, },
	},
	
	.start = 0 + 1,
	
	.accepting = (bool*) &(bool[]) {
		[0] = false, // trap state
		[1 + 1] = true,
		[2 + 1] = true,
		[3 + 1] = true,
		[4 + 1] = true,
		[5 + 1] = true,
		[6 + 1] = true,
		[7 + 1] = true,
		[9 + 1] = true,
		[10 + 1] = true,
		[11 + 1] = true,
		[12 + 1] = true,
		[13 + 1] = true,
		[16 + 1] = true,
	},
	
	.values = (unsigned*) &(unsigned[]) {
		[1 ... 17] = 1,
	},
};



