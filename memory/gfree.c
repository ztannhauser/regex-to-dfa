
#undef DEBUGGING
#define DEBUGGING 0

#include <stdbool.h>

#include <debug.h>

#include "header.h"
#include "gconsider.h"
#include "gfree.h"

void gfree(void* ptr)
{
	struct gmemory_header* header;
	ENTER;
	
	if (ptr)
	{
		header = ptr - sizeof(*header);
		
		if (!--header->refcount)
			gconsider(header);
		else
			dpv(header->refcount);
	}
	
	EXIT;
}















