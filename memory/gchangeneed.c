
#undef DEBUGGING
#define DEBUGGING 0

#include <debug.h>

#include "gnoneed.h"
#include "gneeds.h"
#include "gchangeneed.h"

int gchangeneed(void* inneed, void* unneeded, void* needed)
{
	int error = 0;
	ENTER;
	
	dpv(inneed);
	dpv(unneeded);
	dpv(needed);
	
	if (unneeded != needed)
	{
		error = gneeds(inneed, needed);
		
		if (!error)
			gnoneed(inneed, unneeded);
	}
	
	EXIT;
	return error;
}

