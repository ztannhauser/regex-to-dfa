
/*#undef DEBUGGING*/
/*#define DEBUGGING 0*/

#include <inttypes.h>
#include <stdio.h>
#include <search.h>
#include <stdarg.h>

#include <debug.h>

#include "header.h"
#include "gdigraph.h"

#if DEBUGGING

static int compare(const void* a, const void* b)
{
	return ((void*) a) != *((void**) b);
}

static const void* find(const void *key, const void *base, size_t nmemb, size_t size, int(*compar)(const void *, const void *))
{
	const uint8_t (*ele)[size] = base;
	
	while (nmemb--)
		if (!compar(key, ele))
			return ele;
		else
			ele++;
	
	return NULL;
}

static void gdigraph_helper(FILE* fout, void* ptr)
{
	ENTER;
	
	struct {
		struct gmemory_header** data;
		size_t n, cap;
	} buffer = {NULL, 0, 0};
	
	void helper(struct gmemory_header* header)
	{
		size_t i, n;
		if (!find(header, buffer.data, buffer.n, sizeof(buffer.data[0]), compare))
		{
			// push:
			{
				if (buffer.n + 1 > buffer.cap)
					buffer.data = realloc(buffer.data, sizeof(buffer.data[0]) * (buffer.cap = buffer.cap * 2 ?: 1));
				
				buffer.data[buffer.n++] = header;
			}
			
			fprintf(fout, "\t\"%p\" [label=\"refcount: %u\"]\n", header, header->refcount);
			
			for (i = 0, n = header->thoseineed.n; i < n; i++)
			{
				fprintf(fout, "\t\"%p\" -> \"%p\"\n", header, header->thoseineed.headers[i]);
			}
			
			#if 0
			for (i = 0, n = header->thosewhoneedme.n; i < n; i++)
			{
				fprintf(fout, "\t\"%p\" -> \"%p\"\n", header->thosewhoneedme.headers[i], header);
			}
			#endif
			
			for (i = 0, n = header->thoseineed.n; i < n; i++)
			{
				helper(header->thoseineed.headers[i]);
			}
		}
	}
	
	struct gmemory_header* header;
	
	header = ptr - sizeof(*header);
	
	helper(header);
	
	free(buffer.data);
	
	EXIT;
}

void gdigraph(void* ptr)
{
	ENTER;
	
	FILE* fout = fopen("/tmp/digraph.gv", "w+");
	
	assert(fout);
	
	fprintf(fout, "digraph {\n");
	
	gdigraph_helper(fout, ptr);
	
	fprintf(fout, "}\n");
	
	fclose(fout);
	
	assert(!system("dot /tmp/digraph.gv -Tpng > /tmp/digraph.png"));
	
	EXIT;
}

void gdigraph2(void* ptr, ...)
{
	va_list ap;
	ENTER;
	
	va_start(ap, ptr);
	
	FILE* fout = fopen("/tmp/digraph.gv", "w+");
	
	assert(fout);
	
	fprintf(fout, "digraph {\n");
	
	do gdigraph_helper(fout, ptr);
	while ((ptr = va_arg(ap, void*)));
	
	fprintf(fout, "}\n");
	
	fclose(fout);
	
	assert(!system("dot /tmp/digraph.gv -Tpng > /tmp/digraph.png"));
	
	va_end(ap);
	EXIT;
}

#endif












