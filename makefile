

TARGET = release

CC = gcc

CPPFLAGS += -I .

ifeq ($(TARGET), release)
CPPFLAGS += -D DEBUGGING=0
else
CPPFLAGS += -D DEBUGGING=1
endif

CFLAGS += -Wall -Werror

ifeq ($(TARGET), release)
CFLAGS += -O2
CFLAGS += -flto
else
CFLAGS += -g
CFLAGS += -Wno-unused-variable
CFLAGS += -Wno-unused-function
CFLAGS += -Wno-unused-but-set-variable
endif

LDLIBS += -lavl

default: bin/regex-to-dfa.$(TARGET)

bin:
	rm -f ./bin
	ln -s `mktemp -d` bin
	find -type d | sed 's ^ bin/ ' | xargs -d \\n mkdir -p

bin/srclist.mk: | bin
	find -name '*.c' ! -name '*-out.c' | sed 's/^/SRCS += /g' > $@

include bin/srclist.mk

OBJS = $(patsubst %.c,bin/%.$(TARGET).o,$(SRCS))
DEPS = $(patsubst %.c,bin/%.$(TARGET).d,$(SRCS))

ARGS += ./example1.regex -o ./example1-out.c
#ARGS += ./example2.regex -o ./example2-out.c
#ARGS += ./example3.regex -o ./example3-out.c -t trap
#ARGS += ./example4.regex -o ./example4-out.c

run: bin/regex-to-dfa.$(TARGET)
	$< $(ARGS)

valrun: bin/regex-to-dfa.$(TARGET)
	valgrind $< $(ARGS)

valrun-leak: bin/regex-to-dfa.$(TARGET)
	valgrind --leak-check=full $< $(ARGS)

outs: *-out.c

%-out.c: bin/regex-to-dfa.$(TARGET) %.regex 
	$^ -o $@

install: ~/bin/regex-to-dfa

~/bin/regex-to-dfa: bin/regex-to-dfa.release
	cp -vf $< $@

bin/regex-to-dfa.$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

bin/%.$(TARGET).o bin/%.$(TARGET).d: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -MMD -o bin/$*.$(TARGET).o || (gedit $<; false)

include $(DEPS)


















