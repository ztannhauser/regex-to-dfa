
#include <debug.h>

#include <memory/gmalloc.h>
#include <memory/gneeds.h>
#include <memory/ginc.h>
#include <memory/gfree.h>

#include "struct.h"
#include "new.h"

void delete_state(void* ptr)
{
	ENTER;
	
	dpv(ptr);
	
	struct state* this = ptr;
	
	gfree(this->transitions.data);
	gfree(this->lambda_transitions.data);
	
	EXIT;
}

int new_state(struct state** retval)
{
	int error = 0;
	struct state* this = NULL;
	ENTER;
	
	error = gmalloc((void**) &this, sizeof(*this), delete_state);
	
	
	if (!error)
	{
		this->is_accepting = false;
		
		this->value = 1;
		
		this->phase = p_intialized;
		
		this->default_transition_to = NULL;
		
		this->transitions.data = NULL;
		this->transitions.cap = 0;
		this->transitions.n = 0;
	}
	
	if (!error)
		*retval = ginc(this);
	
	gfree(this);
	
	EXIT;
	return error;
}

















