
#include <inttypes.h>
#include <wchar.h>
#include <stdbool.h>

struct state
{
	unsigned value;
	
	bool is_accepting;
	
	struct {
		struct transition {
			unsigned value;
			struct state* to;
		}** data;
		size_t n, cap;
	} transitions;
	
	struct {
		struct state** data;
		size_t n, cap;
	} lambda_transitions;
	
	struct state* default_transition_to;
	
	enum {
		p_intialized,
		s_flattened,
	} phase;
};

