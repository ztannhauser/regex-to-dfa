
#include <debug.h>

#include <memory/delete.h>
#include <memory/gneeds.h>

#include "struct.h"
#include "set_default_transition.h"

int state_set_default_transition(struct state* here, struct state* there)
{
	int error = 0;
	ENTER;
	
	assert(here);
	
	if (here->default_transition_to != there)
	{
		delete(here->default_transition_to);
		
		here->default_transition_to = there;
		
		error = gneeds(here, there);
	}
	
	EXIT;
	return error;
}

