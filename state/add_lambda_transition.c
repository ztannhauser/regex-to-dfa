
#include <debug.h>

#include <memory/grealloc.h>
#include <memory/gneeds.h>

#include "struct.h"
#include "add_lambda_transition.h"

int state_add_lambda_transition(struct state* from, struct state* to)
{
	int error = 0;
	ENTER;
	
	if (from->lambda_transitions.n + 1 > from->lambda_transitions.cap)
		error = grealloc(
			(void**) &from->lambda_transitions.data,
			sizeof(from->lambda_transitions.data[0]) * (
				from->lambda_transitions.cap = from->lambda_transitions.cap * 2 ?: 1));
	
	if (!error)
		from->lambda_transitions.data[from->lambda_transitions.n++] = to;
	
	if (!error)
		error = gneeds(from, to);
	
	EXIT;
	return error;
}












