
#include "write_header.h"

void write_header(FILE* fout, const char* dfa_name)
{
	fprintf(fout, ""
	"\n"
	"\n"
	"#include <wchar.h>\n"
	"#include <stdbool.h>\n"
	"#include <inttypes.h>\n"
	"\n"
	"\n"
	"#ifndef _DFA_TUPLE_\n"
	"#define _DFA_TUPLE_\n"
	"struct dfa_tuple {\n"
		"\t""// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)\n"
		"\t""unsigned max_state;    // Q = {0 ... max_state}\n"
		"\t""unsigned max_alphabet; // Σ = {0 ... max_alphabet}\n"
		"\t""unsigned **table;      // δ = (state, letter) -> (state)\n"
		"\t""unsigned start;        // s = starting_state\n"
		"\t""bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}\n"
		"\t""\n"
		"\t""unsigned *values;      // unsigned values[state];\n"
	"};\n"
	"#endif // ifndef _DFA_TUPLE_\n"
	"\n"
	"\n"
	"extern struct dfa_tuple %s;\n\n\n", dfa_name);
}

