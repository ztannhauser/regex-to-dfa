
#include <stdlib.h>

#include <debug.h>

#include <state/struct.h>

#include "traverse.h"

int traverse(struct state* start_node, int phase, int (*callback)(struct state* node))
{
	int func(struct state* node)
	{
		int error = 0;
		size_t i, n;
		
		if (node->phase != phase)
		{
			node->phase = phase;
			
			error = callback(node);
			
			for (i = 0, n = node->transitions.n; !error && i < n; i++)
				error = func(node->transitions.data[i]->to);
			
			if (!error && node->default_transition_to)
				error = func(node->default_transition_to);
		}
		
		return error;
	}
	
	return func(start_node);
}

