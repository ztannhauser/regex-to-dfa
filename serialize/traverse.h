
struct state;

int traverse(struct state* start_node, int state, int (*callback)(struct state* node));
