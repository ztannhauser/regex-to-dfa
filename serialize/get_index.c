
#include <search.h>

#include <stdlib.h>

#include "compare.h"
#include "get_index.h"

size_t get_index(struct state** nodes, size_t n, struct state* findme)
{
	return (((struct state**) bsearch(&findme, nodes, n, sizeof(nodes[0]), compare)) - nodes);
}

