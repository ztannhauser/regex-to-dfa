
#include <macros/cmp.h>

#include "compare.h"

int compare(const void* a, const void* b)
{
	return cmp(*((const void**) a), *((const void**) b));
}

