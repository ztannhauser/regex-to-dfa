
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/limits.h>

#include <error_codes.h>
#include <debug.h>

#include <macros/max.h>

/*#include <memory/gmalloc.h>*/
/*#include <memory/gneeds.h>*/
#include <memory/delete.h>

#include <state/struct.h>
/*#include <state/encode_utf8_name.h>*/

/*#include <utf8/encode_string.h>*/

#include "traverse.h"
#include "write_header.h"
#include "flatten.h"
#include "get_index.h"
#include "serialize.h"

int serialize(
	struct state* start_state,
	const char* path,
	bool should_write_header,
	const char* dfa_name,
	const char* trap_state_name)
{
	int error = 0;
	ENTER;
	
	dfa_name = dfa_name ?: "converted_dfa";
	
	FILE* fout = NULL;
	
	if (!(fout = fopen(path, "w+")))
		perror("fopen"),
		error = e_syscall_failed;
	
	FILE* hout = NULL;
	
	char* suffix;
	char header_path[PATH_MAX];
	
	if (!error)
	{
		if (should_write_header)
		{
			strcpy(rindex(strcpy(header_path, path), '.'), ".h");
			
			dpvs(header_path);
			
			if (!(hout = fopen(header_path, "w+")))
				perror("fopen"),
				error = e_syscall_failed;
			
			write_header(hout, dfa_name);
			
			suffix = rindex(header_path, '/');
			
			fprintf(fout, "#include \"%s\"\n", suffix ? suffix + 1 : header_path);
		}
		else
		{
			write_header(fout, dfa_name);
		}
	}
	
	struct state** states = NULL, *state;
	struct transition* transition;
	
	size_t max_alphabet = 0, n_states = 0;
	
	if (!error)
		error = flatten(&states, &n_states, start_state);
	
	size_t i, n;
	size_t j, m;
	
	// find max_alphabet:
	if (!error)
	{
		for (i = 0, n = n_states; i < n; i++)
			for (state = states[i], j = 0, m = state->transitions.n; j < m; j++)
				max_alphabet = max(max_alphabet, state->transitions.data[j]->value);
	}
	
	if (!error)
	{
		fprintf(fout, "struct dfa_tuple %s = {\n", dfa_name);
		
		// print max_state:
		{
			fprintf(fout, "\t.max_state = %lu,\n", n_states);
			fprintf(fout, "\t\n");
		}
		
		// print max_alphabet:
		{
			fprintf(fout, "\t.max_alphabet = %lu,\n", max_alphabet);
			fprintf(fout, "\t\n");
		}
		
		// print table:
		{
			fprintf(fout, "\t.table = (unsigned**) &(unsigned*[]) {\n");
			
			fprintf(fout, "\t\t[0]     = (unsigned*) &(unsigned[]) {[0 ... %lu + 1] = 0}, // trap state\n", max_alphabet);
			
			for (i = 0, n = n_states; i < n; i++)
			{
				state = states[i];
				
				fprintf(fout, "\t\t[%lu + 1] = (unsigned*) &(unsigned[]) {", i);
				
				if (state->default_transition_to)
					fprintf(fout, "[0 ... %lu + 1] = %lu + 1, ", max_alphabet, get_index(states, n_states, state->default_transition_to));
				else
					fprintf(fout, "[%lu + 1] = 0, ", max_alphabet);
				
				for (j = 0, m = state->transitions.n; j < m; j++)
				{
					transition = state->transitions.data[j];
					
					fprintf(fout, "[%u] = %lu + 1, ", transition->value, get_index(states, n_states, transition->to));
				}
				
				fprintf(fout, "},\n");
			}
			
			fprintf(fout, "\t},\n");
			fprintf(fout, "\t\n");
		}
		
		// print start
		{
			fprintf(fout, "\t.start = %lu + 1,\n", get_index(states, n_states, start_state));
			fprintf(fout, "\t\n");
		}
		
		// print accepting:
		{
			fprintf(fout, "\t.accepting = (bool*) &(bool[]) {\n");
			
			fprintf(fout, "\t\t[0] = false, // trap state\n");
			
			for (i = 0, n = n_states; i < n; i++)
				if (state = states[i], state->is_accepting)
					fprintf(fout, "\t\t[%lu + 1] = true,\n", i);
			
			fprintf(fout, "\t},\n");
			fprintf(fout, "\t\n");
		}
		
		// print values:
		{
			fprintf(fout, "\t.values = (unsigned*) &(unsigned[]) {\n");
			
			fprintf(fout, "\t\t[1 ... %lu] = 1,\n", n_states);
			
			for (i = 0, n = n_states; i < n; i++)
				if (state = states[i], state->value != 1)
					fprintf(fout, "\t\t[%lu + 1] = %u,\n", i, state->value);
			
			fprintf(fout, "\t},\n");
		}
		
		fprintf(fout, "};\n");
		
		fprintf(fout, "\n\n\n");
	}
	
	if (fout)
		fclose(fout);
	
	if (hout)
		fclose(hout);
	
	delete(states);
	
	EXIT;
	return error;
}



























