
struct state;

int flatten(
	struct state*** outgoing_nodes, size_t* outgoing_n,
	struct state* start_node);
