
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <error_codes.h>
#include <debug.h>

#include <memory/gmalloc.h>
#include <memory/delete.h>
#include <memory/ginc.h>

#include "struct.h"
#include "process_flags.h"

int process_flags(struct cmdln_flags** retval, int argc, char *const *argv)
{
	int error = 0;
	int opt;
	ENTER;
	
	const char* infile = NULL;
	const char* outfile = NULL;
	const char* dfa_name = NULL;
	const char* trap_state_name = NULL;
	
	bool write_header = false;
	
	while (!error && (opt = getopt(argc, argv, "hn:o:t:")) != -1)
	{
		switch (opt)
		{
			case 'h':
				write_header = true;
				break;
			
			case 'n':
				dfa_name = optarg;
				break;
				
			case 'o':
				outfile = optarg;
				break;
			
			case 't':
				trap_state_name = optarg;
				break;
			
			default: /* '?' */
			{
				fprintf(stderr, "unknown flag!\n");
				error = e_bad_command_line_args;
				break;
			}
		}
	}
	
	if (!error && !outfile)
	{
		fprintf(stderr, "output file path not given!\n");
		error = e_bad_command_line_args;
	}
	
	if (!error && !rindex(outfile, '.'))
	{
		fprintf(stderr, "output file path needs a file extension!\n");
		error = e_bad_command_line_args;
	}
	
	if (!error && optind >= argc)
	{
		fprintf(stderr, "input file path not given!\n");
		error = e_bad_command_line_args;
	}
	
	if (!error)
		infile = argv[optind];
	
	struct cmdln_flags* flags = NULL;
	
	if (!error)
		error = gmalloc((void**) &flags, sizeof(*flags), NULL);
	
	if (!error)
	{
		flags->infile = infile;
		flags->outfile = outfile;
		flags->dfa_name = dfa_name;
		flags->write_header = write_header;
		flags->trap_state_name = trap_state_name;
		
		dpvs(infile);
	}
	
	if (!error)
		*retval = ginc(flags);
	
	delete(flags);
	
	EXIT;
	return error;
}

















