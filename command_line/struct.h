
#include <stdbool.h>

struct cmdln_flags
{
	const char* infile;
	const char* outfile;
	const char* dfa_name;
	const char* trap_state_name;
	bool write_header;
};

