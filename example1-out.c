

#include <wchar.h>
#include <stdbool.h>
#include <inttypes.h>


#ifndef _DFA_TUPLE_
#define _DFA_TUPLE_
struct dfa_tuple {
	// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)
	unsigned max_state;    // Q = {0 ... max_state}
	unsigned max_alphabet; // Σ = {0 ... max_alphabet}
	unsigned **table;      // δ = (state, letter) -> (state)
	unsigned start;        // s = starting_state
	bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}
	
	unsigned *values;      // unsigned values[state];
};
#endif // ifndef _DFA_TUPLE_


extern struct dfa_tuple converted_dfa;


struct dfa_tuple converted_dfa = {
	.max_state = 5,
	
	.max_alphabet = 99,
	
	.table = (unsigned**) &(unsigned*[]) {
		[0]     = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 0}, // trap state
		[0 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 4 + 1, [97] = 1 + 1, },
		[1 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 4 + 1, [97] = 1 + 1, [98] = 2 + 1, },
		[2 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 4 + 1, [97] = 1 + 1, [99] = 3 + 1, },
		[3 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 4 + 1, [97] = 1 + 1, },
		[4 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 4 + 1, [97] = 1 + 1, },
	},
	
	.start = 0 + 1,
	
	.accepting = (bool*) &(bool[]) {
		[0] = false, // trap state
		[3 + 1] = true,
	},
	
	.values = (unsigned*) &(unsigned[]) {
		[1 ... 5] = 1,
		[3 + 1] = 11,
	},
};



