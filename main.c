
#include <stdio.h>

#include <debug.h>

#include <memory/gdigraph.h>

#include <command_line/struct.h>
#include <command_line/process_flags.h>

#include <parse/parse.h>

#include <convert/convert.h>

#include <serialize/serialize.h>

#include <memory/delete.h>

#if DEBUGGING
int debug_depth;
#endif

int main(int argc, char *const * argv)
{
	int error = 0;
	struct cmdln_flags* flags = NULL;
	struct state* nfa_start_state = NULL;
	struct state* dfa_start_state = NULL;
	ENTER;
	
	error = 0
		?: process_flags(&flags, argc, argv)
		?: parse(&nfa_start_state, flags->infile)
		?: convert(nfa_start_state, &dfa_start_state)
		?: serialize(
			dfa_start_state, flags->outfile, flags->write_header,
			flags->dfa_name, flags->trap_state_name)
		;
	
/*	gdigraph(nfa_start_state);*/
/*	CHECK;*/
	
/*	gdigraph(dfa_start_state);*/
/*	CHECK;*/
	
	delete(nfa_start_state);
	delete(dfa_start_state);
	delete(flags);
	
	EXIT;
	return error;
}
























